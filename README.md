# M321 - Verteilte Systeme programmieren
**Modulidentifikation:** https://www.modulbaukasten.ch/module/321

**Modulentwicklung ZH:** https://gitlab.com/modulentwicklungzh/cluster-api/m321

**Modulverantwortlicher:** Stefan Kemper ([stefan.kemper@tbz.ch](mailto:stefan.kemper@tbz.ch))

<!-- 
TODOs:
* Bezug zu Kompetenzraster herstellen
//-->

## Überblick
Dieses Modul umfasst die folgenden Inhalte:
- **[01 Aufbau verteilter Systeme:](./01%20Aufbau%20verteilter%20Systeme)** Ein Überblick über die Systemkomponenten und den grundsätzlichen Aufbau bzw. das Zusammenspiel der einzelnen Komponenten.
- **[02 Lokale Entwicklungsumgebung:](./02%20Lokale%20Entwicklungsumgebung)** Informationen dazu wie eine lokale Entwicklungsumgebung für die Entwicklung und das Testing von verteilten Systemen aufgebaut werden kann.
- **[03 Migration zu verteiltem System:](03%20Migration%20zu%20verteiltem%20System)** Wie eine monolithische Applikation in ein verteiltes System überführt werden kann. Es werden einige mögliche Vorgehensweisen erklärt.
- **[04 Datenhaltung:](./04%20Datenhaltung)** Bei verteilten Systemen ist die persistente Datenspeicherung eine besondere Herausforderung. Dieses Kapitel beschäftigt sich damit, wie diese Herausforderung gemeistert werden kann.
- **[05 Datenaustausch:](./05%20Datenaustausch)** Ein Überblick über gebräuchliche Datenaustauschprotokolle wird gegeben. Es wird darauf eingegangen wie Schnittstellen zwischen den Systemkomponenten geplant und dokumentiert werden.
- **[06 Sicherheitsaspekte:](./06%20Sicherheitsaspekte)** Alle sicherheitsrelevanten Themen sind in diesem Kapitel gebündelt.
- **[07 Skalierung:](./07%20Skalierung)** Bei der Skalierung geht es darum ein verteiltes System an die Anzahl der Nutzer anzupassen. Ebenfalls ist Hochverfügbarkeit (High Availability oder HA) ein Thema.
- **[08 Monitoring und Logging, Testing und Debugging:](./08%20Monitoring%20und%20Logging,%20Testing%20und%20Debugging)** Ist ein verteiltes System am Laufen, dann muss dieses überwacht werden. Treten Fehler oder Performance-Probleme auf muss die Ursache herausgefunden und Massnahmen für die Behebung geplant und umgesetzt werden.

Durch die erfolgreiche Absolvierung dieses Moduls erreichen Sie die unter [Kompetenzmatrix](./Kompetenzmatrix.md) angegebenen Kompetenzen. Die erreichten Kompetenzen werden durch [Leistungsbeurteilungen](./Leistungsbeurteilung.md) bewertet.

<!--
### Überblick Übungsaufgaben
...
//-->

## Abhängigkeiten zu anderen Modulen
Im Zusammenhang mit verteilten Systemen sind einige der relevanten Inhalte, bereits durch andere Module abgedeckt:
- Kryptographie (Grundlage für sichere Kommunikation zwischen Komponenten von verteilten Systemen): [Modul 114](https://www.modulbaukasten.ch/module/114), [Modulunterlagen TBZ](https://gitlab.com/ch-tbz-it/Stud/m114)
- Authentisierung und Autorisierung (Stichwort IAM): [Modul 183](https://www.modulbaukasten.ch/module/183), [Modulunterlagen TBZ](https://gitlab.com/ch-tbz-it/Stud/m183)
- Datenschutz (& Verschlüsselung): [Modul 231](https://www.modulbaukasten.ch/module/231), [Modulunterlagen TBZ](https://gitlab.com/ch-tbz-it/Stud/m231)
- Verteilte Systeme in der Cloud (horizontale- & vertikale Skalierung): [Modul 346](https://www.modulbaukasten.ch/module/346), [Modulunterlagen TBZ](https://gitlab.com/ch-tbz-it/Stud/m346)
- Architekturen von verteilten Systemen (insbesondere microservice-Architektur), Container und Virtualisierung: [Modul 347](https://www.modulbaukasten.ch/module/347), [Modulunterlagen TBZ](https://gitlab.com/ch-tbz-it/Stud/m347)
- Applikationen testen: [Modul 450](https://www.modulbaukasten.ch/module/450), [Modulunterlagen TBZ](https://gitlab.com/ch-tbz-it/Stud/m450/m450)

Die dort behandelten Themen werden im Modul 321 nochmals repetiert und wenn nötig weiter vertieft.

## Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
