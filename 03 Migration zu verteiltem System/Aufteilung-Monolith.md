# Übungsaufgabe - Aufteilung eines Monoliths
[TOC]

## Lernziele
Sie lernen aus einer Beschreibung einer Applikation zu erkennen, welche logisch zusammengehörenden Einheiten die Applikation enthält und wie sich diese optimal in Microservices aufteilen lässt.

## Rahmenbedingungen
* **Sozialform:** Einzelarbeit oder Tandem
* **Dauer:** 45 Min (1 Lektionen)

## Aufgabenstellung
Gegeben ist eine Beschreibung einer LMS-Applikation (Quelle: ChatGPT):

---

Ein Learning Management System (LMS) ist eine digitale Plattform, die es ermöglicht, Lerninhalte effizient zu verwalten und Lernprozesse zu strukturieren. Sie richtet sich sowohl an Lehrende, die Inhalte bereitstellen, als auch an Lernende, die diese konsumieren und dabei ihren Lernfortschritt verfolgen können. Zentrale Funktionen eines LMS umfassen die Bereitstellung von Kursmaterialien, die Organisation von Lerninhalten und die Möglichkeit, interaktive Elemente wie Aufgaben oder Prüfungen zu integrieren. Die Plattform unterstützt verschiedene Formate von Lernmaterialien, darunter Videos, Dokumente, Quizze und Aufgaben, die in Modulen organisiert sind, damit Lernende schrittweise voranschreiten können.

Lehrende können Kurse in der Plattform anlegen, Materialien hinzufügen und den Fortschritt der Lernenden beobachten. Sie haben zudem die Möglichkeit, den Zugang zu ihren Kursen zu steuern und verschiedene Lernpfade je nach Teilnehmer zu gestalten. Lernende wiederum erhalten nach der Anmeldung Zugriff auf Kurse, in denen sie ihren individuellen Fortschritt nachverfolgen können. Die Plattform bietet hierfür Mechanismen, um abgeschlossene Aufgaben und bestandene Prüfungen zu speichern, sodass der Lernfortschritt über die Zeit hinweg dokumentiert bleibt.

Das LMS stellt sicher, dass Prüfungen und Aufgaben in einer strukturierten Weise bereitgestellt und verwaltet werden. Lehrende können Tests für verschiedene Kursabschnitte erstellen, die automatisch ausgewertet werden können oder von Hand korrigiert werden müssen, falls freie Antworten verlangt sind. Die Ergebnisse werden gesammelt und für Lernende transparent dargestellt, sodass sie stets ihren aktuellen Stand einsehen können.

Neben dem eigentlichen Lernprozess ist auch die Kommunikation zwischen Lernenden und Lehrenden wichtig. Dafür bietet das LMS Funktionen wie Nachrichten oder Diskussionsforen, über die Studierende Fragen stellen oder Inhalte diskutieren können. Diese Kommunikationsmöglichkeiten fördern den Austausch und machen das Lernen interaktiver. Lehrende können Ankündigungen machen oder Rückmeldungen geben, während Lernende ihre Fragen stellen oder mit anderen in Kontakt treten können.

Für eine effektive Verwaltung bietet die Plattform zahlreiche administrative Funktionen, die den Lehrenden und Administratoren helfen, die Lernprozesse und Benutzer effizient zu managen. Diese beinhalten die Möglichkeit, detaillierte Berichte über die Aktivitäten und den Fortschritt der Lernenden zu erstellen, um die Effektivität der Kurse zu überwachen und gegebenenfalls Anpassungen vorzunehmen.

Zusammengefasst bietet ein LMS eine umfassende Umgebung, in der sowohl die Bereitstellung von Lernmaterialien als auch die Verwaltung der Lernenden, die Durchführung von Prüfungen und die Förderung der Kommunikation innerhalb einer Lerngruppe auf einer einzigen Plattform ablaufen.

---

Diese Applikation wurde als Monolith umgesetzt. Ihre Aufgabe ist es nun, eine Liste aller Features zu erstellen, die diese Applikation enthält. Anschliessend leiten Sie aus den vorhandenen Features ab in welche Microservices Sie eine solche Applikation aufteilen würden. Zeichnen Sie die verschiedenen Microservices auf und verbinden die Services miteinander, die zusammenarbeiten. Sie können die Zeichnung digital (beispielsweise mit [draw.io](https://www.drawio.com/)) oder auf Papier anfertigen.