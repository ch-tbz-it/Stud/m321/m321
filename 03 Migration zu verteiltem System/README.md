# 03 Migration zu verteiltem System
Die Migration hin zu verteilten Systemen ist meist mit der Umstrukturierung der Applikation verbunden. Dabei müssen die Teile identifiziert werden, die eine logische Einheit bilden und diese voneinander so weit wie möglich entkoppelt werden. Üblicherweise liegen in monolithischen Systemen enge Kupplungen zwischen logisch getrennten Teilen vor. Für die Migration hin zu verteilten Systemen müssen die engen Kupplungen gelöst und durch klar definierte Schnittstellen ersetzt werden. Sollen die Teile auch "zeitlich" voneinander entkoppelt werden, können Systeme, wie beispielsweise ein Message Broker, zum Einsatz kommen.

Bei einem Umbau eines monolithischen Systems hin zu einer Microservice-Architektur wird die grundlegende Architektur der Software verändert - also ihr innerer Aufbau. Folgendes Bild zeigt ein generisches Beispiel eines Monolithen (Links) und eines verteilten Systems rechts:

```plantuml
skinparam monochrome true
skinparam packageStyle rectangle
hide circle
hide empty members

class "Monolith" {
  Frontend
  Businesslogik
  Datenhaltung
}

package "Microservice Architektur"{
  package "Microservice A" as A {
    class "Businesslogik" {
    }
    class "Datenhaltung" {
    }
  }

  package "Microservice B" as B {
    class "Frontend" {
    }
  }
  package "Microservice C" as C {
    class "Businesslogik" {
    }
    class "Datenhaltung" {
    }
  }

  A<-->B
  B<-->C
  A<-->C
}
```

Im Monolith sind alle Teile in einer Applikation vereint. Bei der Microservice-Architektur sind die Services (oder auch Features) des verteilten Systems als jeweils ein Microservice gekapselt. So könnte Beispielsweise Microservice A eine Benutzerverwaltung, Microservice C eine Notenverwaltung und Microservice B eine Statistik-Komponente sein, die von der Benutzerverwaltung und der Notenverwaltung die Daten zusammenführt und eine grafische Darstellung zur Verfügung stellt.

Wenn also ein Microservice-Architektur-Ansatz verfolgt wird, dann existieren mehrere Datenhaltungen, mehrere Business-Logiken, mehrere Frontends etc. die aber dann für sich genommen für weniger verantwortlich sind - also kleiner und übersichtlicher sind. Wird eine Applikation in Schichten zerteilt (Frontend, Backend, Datenbank), kann dies zwar bereits ein verteiltes System sein (z. B. wenn die einzelnen Schichten auf unterschiedlichen physischen Rechnern laufen) - eine Microservice-Architektur ist das aber nicht. Folgende Grafik zeigt die Unterschiede:

```plantuml
skinparam monochrome true
skinparam packageStyle rectangle
hide circle
hide empty members

package "Schicht-Architektur" {
  class "Frontend" {}
  class "Businesslogik" {}
  class "Datenhaltung" {}

  Frontend<-->Businesslogik
  Businesslogik<-->Datenhaltung
}

package "Microservice-Architektur"{
  package "Microservice A" as A {
    class "Businesslogik" {
    }
    class "Datenhaltung" {
    }
  }

  package "Microservice B" as B {
    class "Frontend" {
    }
  }
  package "Microservice C" as C {
    class "Businesslogik" {
    }
    class "Datenhaltung" {
    }
  }

  A<-->B
  B<-->C
  A<-->C
}
```

Die Schicht-Architektur belässt die Applikation in der Regel in grösseren Teilen, ist aber in der Regel sehr schnell umgesetzt. Die Vorteile einer Microservice-Architektur können damit aber nicht genutzt werden. Bei der Microservice-Architektur lassen sich die Dienste einzeln skalieren - d. h. die Dienste lassen sich unabhängig voneinander an die tatsächliche Nachfrage anpassen. Dienste, die häufig genutzt werden können hochskaliert und Dienste, die selten genutzt werden runterskaliert werden. Wenn ein Feature beim verteilten System dazu kommen soll, muss dieses bei einer Schicht-Architektur in allen Schichten mit eingeführt werden. Bei Microservices bleibt alles wie es ist, es kommt einfach ein weiterer Microservice dazu (beispielsweise Microservice D), der seine Schnittstelle allen interessierten zur Verwendung anbietet. Wenn beispielsweise nur der Microservice B an der neuen Schnittstelle von Microservice D interessiert ist, muss nur Microservice B zusätzlich angepasst werden, muss aber keineswegs zeitgleich geschehen mit der Aufschaltung des neuen Microservices, sondern kann nachgelagert stattfinden.

Die Microservice-Architektur sorgt dafür, dass viele voneinander unabhängige Teile miteinander interagieren. Fällt eines dieser Teile aus, kann es gut sein, dass das Gesamtsystem dennoch weiter funktioniert und nur gewisse Features gerade nicht verfügbar sind. Das sorgt generell für eine höhere Ausfallsicherheit und Systemstabilität.

Bei den verschiedenen Architekturen gibt es nicht nur "reinrassige" Architekturen. Sie können beispielsweise auch innerhalb eines Microservices (z. B. des Microservice A) nochmals in die Schichten Businesslogik und Datenhaltung aufteilen. Trotzdem bleibt die Gesamtarchitektur des verteilten Systems grundsätzlich eine Microservice-Architektur.


## Beispiel Aufteilung nach Features - Microservice-Architektur
Es liegt ein Lagerhaltungssystem vor, dass auf dem Rechner des Lageristen installiert wurde. Das Lagerhaltungssystem gibt dem Lageristen die Möglichkeit, nach Produkten innerhalb des Lagers zu suchen, neue Produkte dem Lager hinzuzufügen oder Produkte aus dem Lager entfernen. Um die Software an unterschiedliche Lager-Situationen anpassen zu können, lassen sich auch die Anzahl der Regale und Ebenen (sprich die Lagerpositionen) verwalten.

Als Monolith ist Datenhaltung und Logik alles in einer Applikation enthalten. Wenn die Applikation nun auf eine verteilte Applikation umgebaut werden soll, müssen erst die logisch zusammengehörenden Einheiten erkannt werden. Im vorliegenden Beispiel wären dies:
* Lagerpositionen (inkl. Datenhaltung)
* Produktverwaltung (inkl. Produktdaten)
* Logging der Lagerbewegungen (inkl. Logdaten)
  * Einbuchen von angelieferten Produkten
  * Ausbuchen von ausgelieferten Produkten
  * Verändern der Lagerposition von bestehenden Produkten
* Suche nach Produkten

Wichtig bei verteilten Systemen ist, dass die Daten und die Logik die zusammengehört in der Regel gemeinsam losgelöst werden. Üblicherweise werden deshalb mehrere Datenbanken angelegt (Für Lagerpositionen, Produktdaten, Logdaten und gegebenenfalls Suchhistorie jeweils eine). Die Datenbanken und auch die Logik-Teile werden dadurch kleiner und überschaubarer. 

Wichtig: Die Aufteilung ist nur ein möglicher Vorschlag. Die Applikation könnte auch noch weiter bzw. anders unterteilt werden. Es gibt nicht die eine richtige Lösung. Manchmal werden auch Teile vorerst zusammen gelassen und erst später aufgetrennt. So könnte beispielsweise die Lagerposition auch enthalten, welches Produkt aktuell auf Ihr gelagert wird. Es könnte aber auch auf dem Produkt gespeichert werden, auf welcher Lagerposition es sich befindet. Es wäre aber auch denkbar, dass die Information, wo sich ein Produkt befindet, als eigene logische Einheit umgesetzt werden könnte (oder sogar bei der Suche untergebracht würde).

Wie der Monolith aufgeteilt wird, ist abhängig von den Präferenzen der Entwickler, aber auch von den Wünschen des Kunden oder der Strukturierung der Daten selbst. Es gibt hier kein "Patentrezept", dass für die Aufteilung angewendet werden kann, sondern muss von Fall zu Fall entschieden werden.


## Übungsaufgabe - Microservice-Architektur
Um die Aufteilung einer Applikation in Microservices üben und vertiefen zu können, finden Sie unter [Aufteilung-Monolith](Aufteilung-Monolith.md) eine Übungsaufgabe dazu.