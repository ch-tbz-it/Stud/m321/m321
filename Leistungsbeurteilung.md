Die Modulnote setzt sich aus folgenden Teilen zusammen:
- LB1: schriftliche Prüfung oder praktische Einzelarbeit (Schnittstelle zwischen zwei Systemen planen, umsetzen und dokumentieren)
- LB2: Projektarbeit (Gruppenarbeit - jede Person entwickelt einen Teil des verteilten Systems, anschliessend werden die Teile zu einem Gesamtsystem zusammengehängt)

Die Lehrperson wird Sie über alle weiteren Details (inkl. Gewichtung der einzelnen Teile) zu den Leistungsbeurteilungen zur gegebenen Zeit informieren.