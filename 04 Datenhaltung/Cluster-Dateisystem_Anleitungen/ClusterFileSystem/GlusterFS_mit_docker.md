# Anleitung ClusterFS mit Docker

## Create a Docker Network

```
docker network create gluster-net
```

## Create Container
### Node1:
```
docker run -d --privileged --network gluster-net --platform linux/amd64 --name gluster-node1 \
  -v ~/glusterfs/node1:/data \
  -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
  --hostname node1 \
  gluster/gluster-centos
```
### Node2s:
```
docker run -d --privileged --network gluster-net --platform linux/amd64 --name gluster-node2 \
  -v ~/glusterfs/node2:/data \
  -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
  --hostname node2 \
  gluster/gluster-centos
```
### Node3:
```
docker run -d --privileged --network gluster-net --platform linux/amd64 --name gluster-node3 \
  -v ~/glusterfs/node3:/data \
  -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
  --hostname node3 \
  gluster/gluster-centos
  ```

  ## Start *glusterd* in container
  ```
docker exec -it gluster-node1 glusterd
docker exec -it gluster-node2 glusterd
docker exec -it gluster-node3 glusterd
```
## Connect nodes
```
docker exec -it gluster-node1 bash
```
```
gluster peer probe node2
gluster peer probe node3
```

## Check if nodes connected

```
gluster peer status
```

## Create Volume
```
gluster volume create gv0 replica 3 node1:/data/brick1/ node2:/data/brick1/ node3:/data/brick1/ force
```

## Set permissions on every node
```
chown -R root:root /data/brick1
chmod -R 755 /data/brick1
mkdir -p /data/brick1/.glusterfs/indices
chown -R root:root /data/brick1/.glusterfs
chmod -R 755 /data/brick1/.glusterfs
```

## Start volume

```
gluster volume start gv0 force
```

## Check for Y

```
gluster volume status gv0
```

Output:
```
Status of volume: gv0
Gluster process                             TCP Port  RDMA Port  Online  Pid
-------------------------------------------------------------------
Brick node1:/data/brick1                    49157     0          Y       561
Brick node2:/data/brick1                    49157     0          Y       460
Brick node3:/data/brick1                    49157     0          Y       432
Self-heal Daemon on localhost               N/A       N/A        Y       582
Self-heal Daemon on node2                   N/A       N/A        Y       481
Self-heal Daemon on node3                   N/A       N/A        Y       453

Task Status of Volume gv0
-------------------------------------------------------------------
There are no active volume tasks

```

## Mount directory
```
mkdir -p /mnt/glusterfs
```
```
mount -t glusterfs node1:/gv0 /mnt/glusterfs
```

## Test file

```
cd /mnt/glusterfs
echo "Hello World" > testfile.txt
```

Now check if file is on every container (node1, node2, node3)

---
> Done!!!