# **Dokumentation zur Installation von VirtualBox, Vagrant und Git auf Windows**
 
### **1. Einführung**
Diese Anleitung beschreibt die Installation von VirtualBox, Vagrant und Git auf einem Windows-Rechner sowie das Klonen eines GitHub-Repositories, um eine GlusterFS-Umgebung mit Vagrant zu erstellen und zu konfigurieren. Diese Tools ermöglichen das Erstellen und Verwalten von virtuellen Maschinen (VMs) und das Einrichten einer Cluster-Infrastruktur mit minimalem Aufwand.
 
### **2. Voraussetzungen**
- Betriebssystem: Windows 10 oder höher
- Internetverbindung
- Administratorrechte auf dem System
 
---
 
## **3. Installation von VirtualBox**
 
### 3.1. Herunterladen von VirtualBox
- Besuche die offizielle VirtualBox-Website:  
  [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)
- Klicke unter dem Abschnitt "VirtualBox platform packages" auf den **Windows hosts** Link.
- Der Download der Installationsdatei (`.exe`) wird automatisch gestartet.
 
### 3.2. Installation von VirtualBox
- Öffne die heruntergeladene `.exe`-Datei, um den Installationsassistenten zu starten.
- Folge den Anweisungen im Installationsassistenten:
  - Klicke auf "Weiter", um den Standardinstallationspfad und die Optionen zu akzeptieren.
  - Wähle die gewünschten Komponenten aus (die Standardoptionen sind in der Regel ausreichend).
  - Klicke auf "Installieren".
- Während der Installation könnte eine Sicherheitsmeldung erscheinen, dass VirtualBox neue Netzwerkadapter installieren möchte. Bestätige dies mit "Zulassen".
- Nach Abschluss der Installation kannst du VirtualBox starten.
 
---
 
## **4. Installation von Vagrant**
 
### 4.1. Herunterladen von Vagrant
- Besuche die offizielle Vagrant-Website:  
  [https://developer.hashicorp.com/vagrant/downloads](https://developer.hashicorp.com/vagrant/downloads)
- Wähle die Installationsdatei für **Windows** aus und lade sie herunter.
 
### 4.2. Installation von Vagrant
- Führe die heruntergeladene `.msi`-Datei aus, um die Installation zu starten.
- Folge den Anweisungen im Installationsassistenten:
  - Klicke auf "Weiter", um den Standardinstallationspfad zu akzeptieren.
  - Klicke auf "Installieren".
- Nach der Installation kannst du überprüfen, ob Vagrant erfolgreich installiert wurde. Öffne die Eingabeaufforderung (cmd) und gib folgenden Befehl ein:
  ```bash
  vagrant --version
  ```
  Dieser Befehl sollte die installierte Version von Vagrant anzeigen.
 
---
 
## **5. Installation von Git**
 
### 5.1. Herunterladen von Git
- Besuche die Git-Website:  
  [https://git-scm.com/download/win](https://git-scm.com/download/win)
- Der Download der `.exe`-Datei sollte automatisch starten.
 
### 5.2. Installation von Git
- Führe die heruntergeladene `.exe`-Datei aus, um den Installationsprozess zu starten.
- Wähle während der Installation die Standardoptionen aus, es sei denn, spezifische Änderungen sind erforderlich.
- Nach der Installation kannst du Git überprüfen, indem du die Eingabeaufforderung (cmd) öffnest und folgenden Befehl eingibst:
  ```bash
  git --version
  ```
  Dieser Befehl sollte die installierte Git-Version anzeigen.
 
---
 
## **6. GlusterFS mit Vagrant aufsetzen**
 
### 6.1. Klonen des GlusterFS-Repository
Nun, da Git, Vagrant und VirtualBox installiert sind, können wir das GlusterFS-Projekt mit Vagrant einrichten.
 
1. Öffne die Eingabeaufforderung (cmd) oder PowerShell.
2. Klone das GlusterFS-Vagrant-Repository von GitHub:
   ```bash
   git clone https://github.com/halkyon/glusterfs-vagrant.git
   ```
3. Wechsle in das geklonte Verzeichnis:
   ```bash
   cd glusterfs-vagrant
   ```
 
### 6.2. Vagrant-Umgebung starten
1. Führe im Verzeichnis `glusterfs-vagrant` den folgenden Befehl aus, um die Vagrant-Umgebung zu starten:
   ```bash
   vagrant up
   ```
   Dieser Befehl startet den Vagrant-Prozess, der die notwendigen virtuellen Maschinen (VMs) in VirtualBox erstellt und konfiguriert.
 
2. Vagrant wird automatisch die GlusterFS-Umgebung in den VMs einrichten. Dieser Vorgang kann einige Minuten dauern, da die virtuellen Maschinen heruntergeladen und konfiguriert werden müssen.
 
### 6.3. Überprüfung des Clusters
1. Nach dem Starten der VMs kannst du auf eine der Maschinen zugreifen, um den Status des GlusterFS-Clusters zu überprüfen:
   ```bash
   vagrant ssh gluster1
   ```
2. Überprüfe den Status der GlusterFS-Peers:
   ```bash
   sudo gluster peer status
   ```
3. Um Informationen über die erstellten Volumes zu erhalten, führe den folgenden Befehl aus:
   ```bash
   sudo gluster volume info
   ```
 
---
 
## **7. Vagrant-Umgebung verwalten**
 
### 7.1. VMs stoppen
Wenn du die VMs nicht mehr benötigst, kannst du sie mit folgendem Befehl stoppen:
```bash
vagrant halt
```
 
### 7.2. VMs löschen
Um die VMs und alle zugehörigen Daten vollständig zu löschen, verwende diesen Befehl:
```bash
vagrant destroy
```
 
---
 
## **8. Fazit**
Durch diese Anleitung hast du VirtualBox, Vagrant und Git erfolgreich auf deinem Windows-Rechner installiert und eine GlusterFS-Cluster-Umgebung mithilfe von Vagrant eingerichtet. Diese Umgebung ermöglicht dir, mehrere virtuelle Maschinen zu erstellen, die miteinander in einem Cluster kommunizieren, und bietet dir die Möglichkeit, weitere Netzwerkkonfigurationen und Tests durchzuführen.
 
---
 
Das ist die vollständige Dokumentation für die Installation und Einrichtung der notwendigen Tools auf Windows. Du kannst diese Dokumentation für deine Schulaufgabe verwenden. Wenn du zusätzliche Informationen benötigst, stehe ich gerne zur Verfügung!
hat Kontextmenü
