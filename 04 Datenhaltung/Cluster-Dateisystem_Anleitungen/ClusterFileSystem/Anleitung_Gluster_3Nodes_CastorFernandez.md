# Vagrant und GlusterFS Cluster Setup Anleitung
## Was ist Vagrant
Vagrant ist ein Open-Source-Tool zur Verwaltung und Automatisierung von virtuellen Maschinen, das Entwicklern hilft, einheitliche Entwicklungsumgebungen zu erstellen und zu konfigurieren. Es ermöglicht die einfache Bereitstellung und Verwaltung von virtuellen Maschinen mit Hilfe von sogenannten "Boxen" und Konfigurationsdateien
## Vagrant installieren

Bevor du startest, stelle sicher, dass du Vagrant und einen VirtualBox-Provider (oder einen anderen Vagrant-Provider) installiert hast. Du kannst Vagrant von der offiziellen Seite herunterladen:
## Vagrantfile erstellen
Erstelle ein Verzeichnis für dein Projekt und erstelle in diesem Verzeichnis eine Vagrantfile, um deine virtuellen Maschinen (VMs) zu definieren.

```bash
mkdir vagrant-glusterfs-cluster

cd vagrant-glusterfs-cluster
```

Erstelle einen Vagrantfile mit folgenden Inhalt:
  

```ruby

Vagrant.configure("2") do |config|

  config.vm.box = "ubuntu/bionic64"

  glusterfs_version = "7"

  config.vm.boot_timeout = 1000

  # Setting memory and CPUs for each VM

  config.vm.provider "virtualbox" do |vb|

    vb.memory = "2048"

    vb.cpus = 2

  end

 # Wir haben drei Knoten als Gluster-Hosts eingerichtet und einen Gluster-Client, um das Volume zu mounten.

  (1..3).each do |i|

    config.vm.define vm_name = "gluster-server-#{i}" do |config|

      config.vm.hostname = vm_name

      ip = "172.21.12.#{i+10}"

      config.vm.network :private_network, ip: ip

      config.vm.provision :shell, inline: <<-SHELL

        DEBIAN_FRONTEND=noninteractive apt-get update && \

        apt-get install -yq software-properties-common && \

        add-apt-repository ppa:gluster/glusterfs-#{glusterfs_version} && \

        apt-get update && \

        apt-get install -yq glusterfs-server

      SHELL

    end

  end

  config.vm.define vm_name = "gluster-client" do |config|

    config.vm.hostname = vm_name

    ip = "172.21.12.10"

    config.vm.network :private_network, ip: ip

    config.vm.provision :shell, inline: <<-SHELL

      DEBIAN_FRONTEND=noninteractive apt-get update && \

      apt-get install -yq software-properties-common && \

      add-apt-repository ppa:gluster/glusterfs-#{glusterfs_version} && \

      apt-get update && \

      apt-get install -yq glusterfs-client

    SHELL

  end

end
```

## VMs mit Vagrant starten

Um die virtuellen Maschinen zu starten und den GlusterFS-Server auf jeder VM zu installieren, benutze:

```bash
vagrant up
```

Was macht `vagrant up`?

`vagrant up` startet alle in der Vagrantfile definierten VMs. Es lädt das spezifizierte Betriebssystem (Box), konfiguriert das Netzwerk und führt die im Vagrantfile angegebenen befehle aus.
## GlusterFS-Cluster konfigurieren

Nachdem die VMs laufen, bevor wir ein Volume erstellen können, das sich über mehrere Maschinen erstreckt, müssen wir Cluster mitteilen, dass es die anderen Hosts erkennen soll.
```bash
$ vagrant ssh gluster-server-1 -c 'sudo gluster peer probe 172.21.12.12 ; sudo gluster peer probe 172.21.12.13'
```
## Was ist eine Volume
Ein Volume ein zusammenhängender Speicherbereich, der über mehrere Knoten verteilt ist und als eine einzige Einheit verwaltet wird. Es ermöglicht die Speicherung und den Zugriff auf Daten über verschiedene Cluster-Teilnehmer hinweg, um Redundanz und Skalierbarkeit zu gewährleisten.
## Volume erstellen
Jetzt können wir unser Volume erstellen und starten.
```bash
$ vagrant ssh gluster-server-1 -c 'sudo gluster volume create glustertest replica 3 transport tcp 172.21.12.11:/brick 172.21.12.12:/brick 172.21.12.13:/brick force'
```

```bash
$ vagrant ssh gluster-server-1 -c 'sudo gluster volume start glustertest'
```
Hier erstellen wir ein repliziertes Volume über drei Hosts. Die Anzahl der Bricks muss mit der Anzahl der Repliken übereinstimmen

## Volume Mounten
```bash
$ vagrant ssh gluster-client -c 'sudo mkdir /mnt/glusterfs && sudo mount -t glusterfs 172.21.12.11:/glustertest /mnt/glusterfs'
```
Beachte hier, dass du nur einen Host zum Mounten angeben sollst, das liegt daran, dass der Cluster-Client sich verbindet und Metadaten über das Volume abruft.

## Erstellen Test-Daten:
Nun können wir es nutzen als wäre es unser localer Dateisystem:
```bash
$ vagrant ssh gluster-client -c 'echo hello | sudo tee /mnt/glusterfs/f.txt'
```
## Testen
### Was passiert wenn wir eine der VM's herunterfahren ?
```bash
$ vagrant halt gluster-server-1
```

```bash
$ vagrant ssh gluster-client -c 'ls /mnt/glusterfs/'
f.txt  junk  lolol.txt
```
sollte alles noch funktionieren
### Wenn man eine zweite VM herunterfährt?
```bash
$ vagrant halt gluster-server-2
```

```bash
$ vagrant ssh gluster-client -c 'ls /mnt/glusterfs/'
f.txt  junk  lolol.txt
```
Alles sollte noch funktionsfähig sein.

## Wenn etwas nicht funktioniert könnten folgende zwei befehle von nutzen sein:
```bash
sudo gluster peer status
sudo gluster volume info
```
## VMs verwalten

`vagrant up`:

Startet die virtuellen Maschinen, die in der Vagrantfile definiert sind. Wenn die VMs bereits laufen, passiert nichts weiter.

`vagrant halt`:

Fährt die VMs herunter, ähnlich wie ein "shutdown" auf einem physischen Computer.
## VM-Status anzeigen

`vagrant global-status`:

Zeigt eine Übersicht aller laufenden Vagrant-VMs auf deinem Computer, unabhängig von dem aktuellen Verzeichnis. Dies ist nützlich, um den Status aller VMs zu überprüfen.

`vagrant global-status --prune`:

Bereinigt die Liste der VMs, indem VMs entfernt werden, die nicht mehr existieren oder nicht mehr laufen.
## VMs zerstören

`vagrant destroy`:

Löscht die VMs vollständig. Die virtuellen Maschinen werden entfernt und alle damit verbundenen Daten gehen verloren.

## Zusammenfassung der wichtigsten Kommandos:

`Vagrant up`: Startet die VMs und wendet die Konfigurationen aus der Vagrantfile an.

`Vagrant global-status`: Zeigt den Status aller VMs auf deinem Computer an.

`Vagrant global-status --prune`: Bereinigt die Liste und entfernt verwaiste VMs.

`Vagrant halt`: Fährt die VMs herunter.

`Vagrant destroy`: Löscht die VMs vollständig und entfernt alle damit verbundenen Daten.







######
Castor M. Fernández