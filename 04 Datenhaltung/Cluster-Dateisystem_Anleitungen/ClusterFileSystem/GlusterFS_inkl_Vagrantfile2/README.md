# Cluster-Dateisystem

Der Begriff "Cluster-Dateisystem" bezeichnet ein Dateisystem, das es mehreren Computern in einem Cluster ermöglicht, gleichzeitig auf einen gemeinsamen **Shared Storage** zuzugreifen. In meinem Fall habe ich einen Cluster mit 3 Nodes und einem zusätzlichen Monitoring-Node, der für das Management des Clusters sorgt. Die 3 Nodes stellen das Filesystem bereit, welches über Shared Storage verwaltet wird und auf das Lesen, Schreiben und Zugriffe ermöglicht werden.

**Graphische Darstellung des Clusters** ![Clusterfs_architecture.drawio.svg](Clusterfs_architecture.drawio.svg)
## Was ist ein Volume?

Ein Volume in GlusterFS ist eine logische Sammlung von Speicherbausteinen, die als eine einzige Einheit verwaltet werden. Es ermöglicht die Aggregation von Speicherplatz über mehrere Server hinweg, wodurch ein verteiltes Dateisystem entsteht.

Ein Volume besteht aus mehreren “Bricks”, die die grundlegenden Speichereinheiten darstellen. Diese Bricks sind Verzeichnisse auf den Servern im GlusterFS-Cluster. Es gibt verschiedene Arten von Volumes, wie z.B. verteilte (distributed), replizierte (replicated) und erasure-coded Volumes, die jeweils unterschiedliche Vorteile in Bezug auf Leistung und Datensicherheit bieten.

## Was ist ein Mount?
**Mounten von GlusterFS** bedeutet, ein GlusterFS-Volume auf einem Client-Rechner zugänglich zu machen, sodass es wie ein lokales Dateisystem verwendet werden kann. Dies erfolgt durch die Installation des GlusterFS-Clients, das Erstellen eines Verzeichnisses und das Verwenden des `mount`-Befehls, um das Volume zu verbinden.
## Aufschaltung der VMs

### Wichtige Vagrant Befehle

- `vagrant up`: Startet die VMs.
- `vagrant ssh <vmname>`: Zugriff auf die VM via SSH.
- `vagrant global-status`: Zeigt den Status und Überblick der VMs.
- `vagrant global-status --prune`: Zeigt den globalen Status ohne Cache.
- `vagrant destroy`: Löscht eine VM.

Um die VMs (Nodes) zu starten, habe ich [Vagrant](https://www.vagrantup.com/) verwendet. Vagrant ist ein VM-Orchestrierungstool, um die VMs (Nodes) zu verwalten.

Die Konfiguration für die VMs erfolgt durch ein `Vagrantfile`, ähnlich wie bei einem Dockerfile, das für die Konfiguration und Orchestrierung der VMs sorgt.

### Vagrantfile für die GlusterFS-Architektur
```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/bionic64"
  glusterfs_version = "7"
  config.vm.boot_timeout = 1000

  # Setting memory and CPUs for each VM
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
    vb.cpus = 2
  end

  # We setup three nodes to be gluster hosts, and one gluster client to mount the volume
  (1..3).each do |i|
    config.vm.define "gluster-server-#{i}" do |server|
      server.vm.hostname = "gluster-server-#{i}"
      ip = "172.21.12.#{i+10}"
      server.vm.network :private_network, ip: ip

      server.vm.provision :shell, inline: <<-SHELL
        DEBIAN_FRONTEND=noninteractive apt-get update && \
        apt-get install -yq software-properties-common && \
        add-apt-repository ppa:gluster/glusterfs-#{glusterfs_version} && \
        apt-get update && \
        apt-get install -yq glusterfs-server
      SHELL
    end
  end

  config.vm.define "gluster-client" do |client|
    client.vm.hostname = "gluster-client"
    client.vm.network :private_network, ip: "172.21.12.10"

    client.vm.provision :shell, inline: <<-SHELL
      DEBIAN_FRONTEND=noninteractive apt-get update && \
      apt-get install -yq software-properties-common && \
      add-apt-repository ppa:gluster/glusterfs-#{glusterfs_version} && \
      apt-get update && \
      apt-get install -yq glusterfs-client
    SHELL
  end
end

```

### Wichtige Konfigurationen für unser Clustersystem

- **Gluster-Server VMs**:
    
    - **Betriebssystem**: Ubuntu 18.04 (bionic64)
    - **Anzahl der Server**: 3
    - **Hostnamen**: gluster-server-1, gluster-server-2, gluster-server-3
    - **Private IP-Adressen**:
        - gluster-server-1: `172.21.12.11`
        - gluster-server-2: `172.21.12.12`
        - gluster-server-3: `172.21.12.13`
    - **Netzwerk**: Private Netzwerkverbindung (IP-Adressraum `172.21.12.X`)
    - **Provisionierung**:
        - Installation von `software-properties-common`
        - Hinzufügen des GlusterFS-Repositorys für Version 7
        - Installation des GlusterFS-Servers (GlusterFS 7)
- **Gluster-Client VM**:
    
    - **Betriebssystem**: Ubuntu 18.04 (bionic64)
    - **Hostname**: gluster-client
    - **Private IP-Adresse**: `172.21.12.10`
    - **Netzwerk**: Private Netzwerkverbindung (IP-Adresse `172.21.12.10`)
    - **Provisionierung**:
        - Installation von `software-properties-common`
        - Hinzufügen des GlusterFS-Repositorys für Version 7
        - Installation des GlusterFS-Clients (GlusterFS 7)

### GlusterFS Konfiguration

Nach dem Ausführen des Befehls `vagrant up` sollten Sie mit `vagrant global-status` den Status Ihrer VMs überprüfen können.

**Peers hinzufügen**: Bevor Sie ein Volume über mehrere Maschinen erstellen können, müssen Sie Gluster mitteilen, dass es die anderen Hosts erkennen soll:

```shell
vagrant ssh gluster-server-1 -c 'sudo gluster peer probe 172.21.12.12 ; sudo gluster peer probe 172.21.12.13'
```



**Volume erstellen**: Auf einem Gluster-Server (z.B. `gluster-server-1`):

```bash
sudo gluster volume create glustertest replica 3 \ 
172.21.12.11:/data/brick1 \ 
172.21.12.12:/data/brick1 \ 
172.21.12.13:/data/brick1
```

Starten Sie das Volume:

```shell
sudo gluster volume start glustertest
```

**Volume-Mount auf Client**: Auf dem Gluster-Client:

```shell
sudo mkdir -p /mnt/glusterfs/glustertest sudo mount -t glusterfs 172.21.12.11:/glustertest /mnt/glusterfs/glustertest
```
### Fehlerbehebung

- **Mount-Fehlerprotokolle**: Überprüfen Sie die Protokolle auf dem Gluster-Client:
```shell
vagrant ssh gluster-server-1 -c 'sudo gluster peer probe 172.21.12.12 ; sudo gluster peer probe 172.21.12.13'
```
    
- **Verbindung überprüfen**:
```shell
nc -zv 172.21.12.11 24007 nc -zv 172.21.12.12 24007 nc -zv 172.21.12.13 24007
```
   
- **Gluster-Dienste neu starten**:
```shell
    sudo systemctl restart glusterd`
```
- **Systemprotokolle überprüfen**
```
    sudo dmesg | tail sudo journalctl -xe
```


### Problembehebung für Schreibzugriffe

Falls beim Schreiben von Dateien auf das gemountete Volume ein Fehler auftritt, wie in folgendem Beispiel:
```shell
vagrant@gluster-client:/mnt/glusterfs$ sudo echo 'hoi.txt'> hoi.txt -bash: hoi.txt: Permission denied
```

Überprüfen Sie die Berechtigungen und versuchen Sie es als Root-Benutzer erneut:
```shell
vagrant@gluster-client:/mnt/glusterfs$ sudo -i 
root@gluster-client:~# cd /mnt/glusterfs root@gluster-client:/mnt/glusterfs# echo "hoi" > hoi.txt
```
Falls der Zugriff weiterhin verweigert wird, stellen Sie sicher, dass die GlusterFS-Freigaben und Berechtigungen korrekt konfiguriert sind.

## Ausprobieren

Cluster als Filesystem:

```shell
$ vagrant ssh gluster-client -c 'echo hello | sudo tee /mnt/glusterfs/f.txt'
```

Grosse Chuck an Data und die Performance sehen:

```shell
$ vagrant ssh gluster-client -c 'sudo dd if=/dev/urandom of=/mnt/glusterfs/junk bs=64M count=16'
```

## Cluster testen

**Was passiert beim stoppen eines nodes?**

```shell
$ vagrant halt gluster-server-1
```

```shell
vagrant ssh gluster-client -c 'ls /mnt/glusterfs/'
f.txt  junk  lolol.txt
```

Alles funktioniert!

**Was passiert wenn wir noch ein Node runterfahren?**

```shell
$ vagrant halt gluster-server-2
```

```shell
vagrant ssh gluster-client -c 'ls /mnt/glusterfs/'
f.txt  junk  lolol.txt
```

Ales funktioniert trotzdem!


**Die heruntergefahrenen Nodes wieder starten was passiert?**

- In der Regel sollte GlusterFS die Knoten automatisch wieder ins Cluster integrieren, aber es ist ratsam, den Status zu überprüfen und sicherzustellen, dass alle Synchronisierungs- und Reparaturprozesse abgeschlossen sind, um eine vollständige Wiederherstellung der Cluster-Funktionalität zu gewährleisten.