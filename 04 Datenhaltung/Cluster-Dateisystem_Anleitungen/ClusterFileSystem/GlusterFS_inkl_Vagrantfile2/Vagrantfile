# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/bionic64"
  glusterfs_version = "7"
  config.vm.boot_timeout = 1000

  # Setting memory and CPUs for each VM
  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
    vb.cpus = 2
  end

  # We setup three nodes to be gluster hosts, and one gluster client to mount the volume
  (1..3).each do |i|
    config.vm.define vm_name = "gluster-server-#{i}" do |config|
      config.vm.hostname = vm_name
      ip = "172.21.12.#{i+10}"
      config.vm.network :private_network, ip: ip

      config.vm.provision :shell, inline: <<-SHELL
        DEBIAN_FRONTEND=noninteractive apt-get update && \
        apt-get install -yq software-properties-common && \
        add-apt-repository ppa:gluster/glusterfs-#{glusterfs_version} && \
        apt-get update && \
        apt-get install -yq glusterfs-server
      SHELL
    end
  end

  config.vm.define vm_name = "gluster-client" do |config|
    config.vm.hostname = vm_name
    ip = "172.21.12.10"
    config.vm.network :private_network, ip: ip

    config.vm.provision :shell, inline: <<-SHELL
      DEBIAN_FRONTEND=noninteractive apt-get update && \
      apt-get install -yq software-properties-common && \
      add-apt-repository ppa:gluster/glusterfs-#{glusterfs_version} && \
      apt-get update && \
      apt-get install -yq glusterfs-client
    SHELL
  end
end
