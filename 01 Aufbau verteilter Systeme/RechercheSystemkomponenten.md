# Auftrag Recherche Systemkomponenten
In der [Theorie](./README.md) haben Sie die verschiedenen Arten / Kategorien von Systemkomponenten kennengelernt. In diesem Auftrag geht es nun darum, eine Kategorie und die darin zur Verfügung stehenden Softwarelösungen genauer unter die Lupe zu nehmen.

## Ziele
- Ein vertieftes Verständnis in einer Kategorie von Systemkomponenten erlangen
- Einen Überblick über alle bekannten verfügbaren Softwarelösungen für verteilte Systeme erlangen

## Rahmenbedingungen
- **Zeitbudget:** 45-90 Min (abhängig davon wie stark Sie sich vertiefen möchten)
- **Sozialform:** Einzel oder Gruppenarbeit (1-3 Personen)

Die Resultate der Recherche halten Sie gemäss Vorgaben der Lehrperson schriftlich fest.

## Ablauf
1. Bilden Sie selbstständig Gruppen (oder entscheiden Sie sich für eine Einzelarbeit).
2. Entscheiden Sie sich für eine der genannten Arten von Systemkomponenten (siehe [Theorie](./README.md#typische-systemkomponenten-in-einem-verteilten-system))
3. Recherchieren Sie gemeinsam oder alleine welche Softwarelösungen für die gewählte Kategorie existieren
4. Vergleichen Sie die gefundenen Softwarelösungen aufgrund der zur Verfügung stehenden Features, Vor- und Nachteile oder auch allfälligen Einschränkungen (beispielsweise "nur SOAP als Datenaustauschprotokoll unterstützt").
5. Halten Sie die Erkenntnisse gemäss Vorgabe der Lehrperson schriftlich fest und stellen diese allen in der Klasse zur Verfügung (ebenfalls auf die Weise wie durch die Lehrperson vorgegeben).
6. Schauen Sie sich die Erkenntnisse der anderen Personen aus der Klasse an. Notieren Sie für sich spannende Erkenntnisse bei Ihren Kursnotizen oder in Ihrem Lernjournal.

Wenn Sie diesen Auftrag durchgearbeitet haben, sollten Sie eine sehr fundierte Entscheidungsgrundlage besitzen, welche Softwarelösung Sie sinnvollerweise für ein spezifisches Problem innerhalb einer spezifischen verteilten Applikation einsetzen könnten. Erarbeiten Sie den Auftrag im Blick auf das Ziel, dass Sie auch für Ihren zukünftigen Berufsalltag als Klasse eine wertvolle Übersicht schaffen die Ihnen den Arbeitsalltag erleichtert.