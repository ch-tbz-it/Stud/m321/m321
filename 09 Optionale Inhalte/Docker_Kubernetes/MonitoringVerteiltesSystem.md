# Monitoring eines verteilten Systems
[TOC]

## Ziel
Die Überwachung von verteilten Systemen ist zentral, um Probleme (struktureller Art oder Performance) innerhalb des verteilten Systems und deren Ursachen zu erkennen und zu lösen. Wenn Sie mit Containern und Kubernetes arbeiten, dann existieren bereits Möglichkeiten, wie Sie eine Applikation überwachen können - Sie brauchen dafür also keine eigene Überwachung umzusetzen. Ziel dieser Übung ist es, dass Sie eine Überwachung für ein beliebiges verteiltes System mit Lösungsansätzen umsetzen, die frei zur Verfügung stehen (beispielsweise mit Kiali für die Visualisierung und den Datenlieferanten / Umsystemen).

## Rahmenbedingungen
* **Sozialform:** Die Aufgabe kann als Einzelarbeit, zu Zweit oder zu Dritt umgesetzt werden.
* **Dauer:** ca. 2-6 Lektionen (abhängig davon wie vielen technischen Hürden Sie gegenüber stehen werden)

## Vorgehen
Entscheiden Sie sich als erstes, was das Ziel der Überwachung sein soll. Wählen Sie danach aufgrund Ihrer Zielsetzung aus, welche Tools bzw. welchen Tools-Stack Sie ausprobieren möchten:
* **Service-Mesh visualisieren:** Um zu visualisieren, welche Daten wie durch Ihren Kubernetes-Cluster fliessen und anzeigen zu lassen, welche Microservices mit welchen anderen Microservices kommunizieren verwenden Sie am besten Kiali in Kombination mit istio. Eine mögliche Anleitung wie Sie diesen Tools-Stack installieren und ausprobieren können finden Sie unter https://istio.io/latest/docs/setup/getting-started/. Im Zusammenhang mit Kiali werden weitere Tools wie Grafana, Prometheus und Jaeger eingesetzt. Weitere Details dazu finden Sie unter https://kiali.io/docs/configuration/p8s-jaeger-grafana/.
* **Zentrales Logfile-Management:** Um Logfiles aus den verschiedenen Pods zusammenzuführen, durchsuchbar zu machen, schöner darzustellen etc. wird häufig ein sogenannter EFK-Stack bestehend aus Elasticsearch, Fluentd und Kibana eingesetzt. Wenn Sie sich dafür interessieren, diesen Tool-Stack zu installieren, bieten die folgenden Artikel Anleitungen dazu:
  * https://medium.com/@tech_18484/simplifying-kubernetes-logging-with-efk-stack-158da47ce982 
  * https://www.digitalocean.com/community/tutorials/how-to-set-up-an-elasticsearch-fluentd-and-kibana-efk-logging-stack-on-kubernetes
  * https://github.com/jpdecastro/kubernetes-cluster-logging-efk
* **Visualisieren von Performance-Daten:** Für die Sammlung von Daten wird in der Regel Prometheus verwendet. Um die Daten zu aggregieren und anzuzeigen kommt in der Regel Grafana zum Einsatz. Diese Kombination ist auch bei der Visualisierung des Service-Mesh (1. Möglichkeit) im Einsatz. Wenn es Ihnen nur darum geht, Performance-Daten auszuwerten, um beispielsweise herauszufinden, welche Services wie viel Leistung brauchen, dann reichen etwas weniger Tools in Ihrem Stack bereits aus. Die folgenden Artikel erklären wie Sie Prometheus und Grafana auf dem Kubernetes-Cluster installieren:
  * https://medium.com/@muppedaanvesh/a-hands-on-guide-to-kubernetes-monitoring-using-prometheus-grafana-%EF%B8%8F-b0e00b1ae039
  * https://grafana.com/docs/grafana-cloud/monitor-infrastructure/kubernetes-monitoring/configuration/config-other-methods/prometheus/prometheus-operator/
  * https://medium.com/israeli-tech-radar/how-to-create-a-monitoring-stack-using-kube-prometheus-stack-part-1-eff8bf7ba9a9

Wenn Sie sich für ein Ziel entschieden haben, setzen Sie die Installation der entsprechenden Software auf dem Kubernetes-Cluster um. Wichtig: die Anleitungen sind nicht abschliessen. Es kann gut sein, dass gewisse Dinge bei Ihnen nicht genau gleich wie in den Anleitungen funktioniert. Dann fühlen Sie sich frei, selbst nach weiteren Anleitungen / Lösungsansätzen zu suchen. Sollten Sie nicht weiterkommen, melden Sie sich bei der Lehrperson die Sie gerne bei der Lösungsfindung unterstützt.