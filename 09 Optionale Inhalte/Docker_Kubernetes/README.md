# Docker / Kubernetes
[TOC]

## Einleitung / Zielsetzung
Sie haben Docker und Kubernetes bereits im [Modul 347](https://gitlab.com/ch-tbz-it/Stud/m347) kennengelernt. Dieses Kapitel soll das bereits erarbeitete Wissen auffrischen und vertiefen. Die Basics werden hier jedoch nicht mehr besprochen - diese lesen Sie bitte bei den Unterlagen zu [Modul 347](https://gitlab.com/ch-tbz-it/Stud/m347) nach (sofern nötig).

Der Fokus dieses Kapitels liegt bei Kubernetes, weil ein Kubernetes-Cluster auch ein verteiltes System ist.

## Systemarchitektur von Kubernetes
Kubernetes ist ein Cluster für die Orchestrierung / Verwaltung von Containern. Clustering kennen Sie bereits aus dem [Kapitel Skalierung](../../07%20Skalierung). Bei einem Kubernetes-Cluster existiert immer eine sogenannte **Control Plane** (managen den Cluster) und die **Worker-Nodes** (erledigen die eigentliche Arbeit).

![Kubernetes Components](Kubernetes_components.svg)
*Quelle Bild & weitere Details: https://kubernetes.io/docs/concepts/architecture/cloud-controller/*

Ist HA (High Availability) eine Anforderung, die an den Cluster gestellt wird, dann müssen alle Komponenten redundant vorhanden sein - nicht nur die Worker-Nodes, sondern auch die Control Plane (auch Master-Node genannt). Wenn jedoch mehrere Master-Nodes existieren, ist die **Split-Brain**-Problematik wieder vorhanden und muss gelöst werden. Die Lösungsansätze von Kubernetes halten sich auch an die unter [Datenhaltung](../../04%20Datenhaltung/README.md#split-brain-problematik) beschriebenen Ansätze.

Wenn nun aber mehrere Control Planes existieren, dann braucht es zusätzlich eine Lastenverteilung (Loadbalancing) zwischen diesen Control Planes. Das Loadbalancing sorgt dann beim Ausfall eines Control Planes, dass die Last auf die noch verbleibenden Control Planes verteilt wird. Dadurch entsteht aber wieder ein **single-point-of-failure**. Deshalb wird für den Loadbalancer üblicherweise ein Failover eingesetzt (zwei Loadbalancer, der Eine aktiv, der Andere passiv).

![HA-Setup bei Kubernetes](img_topology-ha-arch.png)
*Quelle Bild & weitere Details: https://kubitect.io/latest/examples/ha-cluster/*

Der [Artikel](https://kubitect.io/latest/examples/ha-cluster/), von dem das Bild stammt, erklärt den Aufbau eines HA-Cluster-Setups bei Kubernetes im Detail. Dieses [Youtube-Video](https://www.youtube.com/watch?v=PRsB6HzQ_ss) erklärt die Problematik bezüglich High Availability und auf was alles geachtet werden muss anhand des Beispiels eines Kubernetes-Clusters.

## Systemarchitektur von Software die auf Kubernetes deployed wird
Im vorangehenden Abschnitt wurde erklärt, wie ein Kubernetes-Cluster hochverfügbar gemacht wird. Bei der gezeigten Architektur handelt es sich um die Struktur des Clusters aber nicht um die Struktur der Software, die auf dem Cluster deployed wird. Diese Strukturen und verwendeten Fachbegriffe weichen stark voneinander ab.

Der Artikel https://codeblog.dotsandbrackets.com/kubernetes-example/ erklärt den Aufbau sehr schön mit folgendem Bild (das im Artikel nach und nach aufgebaut wird):
![Logischer Aufbau von Software-Deployments](ingress.webp)

Zusammengefasst wird die Applikation gegen aussen in der Regel über Ingress exponiert. Ingress kann man sich wie die Firewall bei Kubernetes vorstellen. Diese kontrolliert die ein- und ausgehende Kommunikation mit der Applikation die auf dem Cluster läuft. Wie bei Stand-alone-Servern kann auch bei Kubernetes Ingress weg gelassen und direkt Services genutzt werden, um die Applikation gegen aussen zu exponieren (im Entwicklungsumfeld allenfalls sinnvoll). In einer Produktiv-Umgebung sollte jedoch die "Firewall" (Ingress) genutzt werden.

Die Services können von unterschiedlichsten Typen sein. Die folgende Tabelle gibt Ihnen einen Überblick:

| Service-Typ      | Beschreibung                                                                                                                                                                                                                                                                                                     |
|------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ClusterIP        | Dieser Service-Typ generiert eine interne IP-Adresse, die nur innerhalb des Clusters erreichbar ist. D. h. andere Pods können auf diesen Service über die IP zugreifen. Zugriffe ausserhalb des Clusters auf diese IP sind jedoch nicht möglich.                                                                 |
| NodePort         | Der NodePort-Service öffnet auf dem Cluster-Node einen spezifischen Port und verbindet diesen mit den Pods. Ein Zugriff von ausserhalb des Clusters über die IP eines Nodes und den exponierten Port wird so möglich.                                                                                            |
| LoadBalancer     | Der LoadBalancer exponiert die Services über eine Fixe IP-Adresse. Wenn nun ein Node im Hintergrund nicht mehr verfügbar sein sollte, dann merkt der Enduser nichts davon, weil der Loadbalancer den Traffic auf einen verbleibenden Node umleitet. Dieser Service-Typ ist sinnvoll für den produktiven Einsatz. |
| ExternalName     | Mit ExternalName wird ein DNS-Eintrag erstellt, der in der Regel auf einen externen Service verweist, der sich ausserhalb des Clusters befindet. Dieser Typ von Service ist dann sinnvoll, wenn den Pods die Kommunikation mit externen Diensten erleichtert werden soll.                                        |
| Headless Service | Hier wird keine neue IP generiert, sondern via DNS direkt auf die IP eines konkreten Pods verwiesen bzw. der Datenverkehr dahin weitergeleitet. Das kann nützlich sein, wenn direkt mit einem spezifischen Pod kommuniziert werden soll.                                                                         |

Quelle & weitere Details: https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types

## Persistente Datenspeicherung
Um Daten persistent speichern zu können, müssen Speicherlösungen wie beispielsweise GlusterFS oder ein anderes Clusterdateisystem verwendet werden. Kubernetes selbst bietet keine persistente Datenspeicherung an - nur den Weg zum Datenspeicher. Die Ressourcen / persistenten Datenspeicher und wie diese vom Cluster aus erreicht werden können, wird mit **persistent volumes** beschrieben. Wenn ein Pod nun einen persistenten Datenspeicher benötigt, kann er diesen mit **persistent volume claims** beantragen und anschliessend verwenden.

<!--
Notizen (work in progress):
Fokus auf Kubernetes und Clustering / Loadbalancing / Architekturen, die es ermöglichen, Software verteilt zu betreiben.

Service discovery
ev. Fachbegriff Round-Robin erwähnen

Services vom Typ NodePort oder LoadBalancer verwendet, um Dienste ausserhalb von Cluster verfügbar zu machen

## Service discovery bei Kubernetes
Bei Kubernetes werden Applikationen in der Microservice-Architektur gehostet. Jeder Pod bietet dabei einen konkreten Service-Endpunkt an. 

//-->

## Pods und Sidecar-Injection
Ist die Sidecar-Injection konfiguriert, dann wird Kubernetes beim Starten von neuen Pods angewiesen, jeweils weitere "Hilfs-Container" innerhalb des Pods neben dem eigentlichen Container laufen zu lassen. Diese zusätzlichen Container erweitern die Funktionalität der Applikation beispielsweise um Logging- und Monitoring-Funktionalitäten.

Weitere Details: https://kubernetes.io/docs/concepts/workloads/pods/sidecar-containers/

<!--
Notizen (work in progress):

Dinge wie Sidecar-Injection oder Serviceendpunkte mit einflechten (ev. Service discovery)
//-->

## Logging & Monitoring mit Kubernetes
Um den Datenverkehr innerhalb des Clusters sichtbar zu machen und so auch bei allfälligen Problemen die Performance-Engpässe zu sehen oder festzustellen, wo ein Request hängen bleibt, werden Sidecars verwendet, die den Datenverkehr aufzeichnen und Meldung an die zentrale Stelle machen, die die Daten auswertet und für den Administrator aufbereitet und darstellt.

So eine Lösung umfasst selbst diverse Systemkomponenten und ist ebenfalls ein verteiltes System. Eine häufig genutzte Zusammenstellung von Komponenten ist Kiali, Prometheus und Grafana. Dabei wird Prometheus für die Sammlung von Messwerten (Metriken) verwendet. Grafana und Kiali werden dafür eingesetzt diese Messwerte für den Endbenutzer übersichtlich darzustellen. Dabei ist Grafana auf die Darstellung von Charts und Zeitreihen spezialisiert. Kiali ist spezialisiert darauf, die Zusammenhänge der verschiedenen Microservices zu visualisieren. Wird zusätzlich noch istio mit dem envoy-proxy verwendet, dann können auch sicherheitsrelevante Themen visualisiert werden. Weitere Details dazu finden Sie im [Modul 347](https://gitlab.com/ch-tbz-it/Stud/m347/-/tree/main/Kubernetes/Sicherheitsaspekte).

### Übungsaufgabe
Wenn Sie das Thema Logging & Monitoring gerne auch noch praktisch vertiefen möchten, lösen Sie bitte die Übungsaufgabe [Monitoring eines verteilten Systems](./MonitoringVerteiltesSystem.md).

<!--
Notizen (work in progress):
Ev. auch Monitoring mit einflechten (Kiali, Prometheus, Grafana)

//-->