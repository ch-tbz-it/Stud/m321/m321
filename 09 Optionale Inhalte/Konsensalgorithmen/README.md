# Konsensalgorithmen

[TOC]

## Einleitung
Unter einem Konsensalgorithmus wird ein Vorgehen verstanden, der es verschiedenen gleichberechtigten Teilnehmern ermöglicht, einen gemeinsamen Konsens (Einigung / Vorgehen / Absprache) zu finden. Im Zusammenhang mit verteilten Systemen sind Konsensalgorithmen beim Clustering und Failover von entscheidender Bedeutung. Hier müssen sich die verschiedenen Nodes einigen können, wer beispielsweise bei einem Ausfall der Cluster Interconnect Verbindungen (Netzwerkverbindungen zwischen den Nodes im Cluster) aktiv bleibt und wer den Dienst einstellt, damit ein [Split-Brain](../../04%20Datenhaltung/README.md#split-brain-problematik) bzw. Inkonsistente Daten vermieden werden können.

## Byzantinische Fehlertoleranz
Bei der byzantinischen Fehlertoleranz geht es darum, dass die einzelnen gutartigen Knoten in verteilten Systemen trotz bösartiger Knoten (Knoten die Fehlerhafte Informationen liefern) zu einem gemeinsamen Konsens kommen können. Algorithmen die byzantinisch fehlertolerant sind, sind Algorithmen die nicht auf gutartiges Verhalten aller Knoten angewiesen sind, sondern dann noch funktionieren, wenn sich einzelne Knoten falsch / bösartig verhalten.

Wird hingegen ein Algorithmus als "nicht byzantinisch fehlertolerant" bezeichnet, dann hat der Algorithmus Mühe richtig zu funktionieren, wenn nicht von allen Knoten zuverlässige Informationen geliefert werden. In dem Wikipedia-Artikel über die [byzantinische Fehlertoleranz](https://de.wikipedia.org/wiki/Byzantinischer_Fehler) finden Sie weitere Details zu der Thematik.

Fakt ist, dass nicht alle Konsensalgorithmen byzantinisch fehlertolerant sind. Wenn eine hohe Stabilität / Resilienz des verteilten Systems wichtig ist, dann sollte auf Algorithmen gesetzt werden, die byzantinisch fehlertolerant sind.

**Wichtig:** Selbst wenn ein Algorithmus tolerant gegen sich falsch verhaltende Knoten reagiert: sobald eine Mehrheit der Knoten sich falsch / unehrlich verhält, kommt auch ein byzantinisch fehlertoleranter Algorithmus an seine Grenzen. Die Blockchain setzt beispielsweise darauf, dass mehr als 50% aller Blockchain-Teilnehmer einen neuen Block als richtig bestätigen, bevor dieser der Block der Chain angehängt werden darf. Wenn nun aber über die Hälfte der Teilnehmer "Verräter" sind und einen gefälschten Block als korrekt validieren, dann kann die Fälschung als solche nicht mehr erkannt werden. Mehr Details dazu sind in der [Blockchain-Theorie](../Blockchain) nachzulesen.

## Beispiele von Konsensalgorithmen
### Raft
Raft wurde als einfachere Alternative zu [Paxos](#paxos) entwickelt.

Bei Raft wird durch ein Voting ein Leader bestimmt, der in der Folge alle wichtigen Entscheide trifft. Jeder Teilnehmer im Cluster kann jedoch eine Neuwahl auslösen, wenn er nach einer gewissen Zeit des wartens nichts mehr vom Leader gehört hat. Der Leader seinerseits sendet Regelmässig Pakete an alle Teilnehmer im Cluster raus um die Verbindung zu den Teilnehmern zu überprüfen und Ausfälle einzelner Teilnehmer oder teilen des Netzwerks diagnostizieren zu können.

Ein Leader wird dann gewählt, wenn er von mehr wie 50% des Netzwerks die Bestätigung kriegt, dass er Leader sein darf. Die Funktionsweise des Algorithmus lässt sich am einfachsten durch die [Raft Visualisierung](https://raft.github.io/) begreifen.

[![Screenshot der Raft Visualisierung](raft_visualisierung.png)](https://raft.github.io/)

Zu beachten: Raft ist nicht byzantinisch fehlertolerant, weil die Nodes dem gewählten Leader vertrauen (Quelle: https://en.wikipedia.org/wiki/Raft_(algorithm)).

### Paxos
Paxos ist im Gegensatz zu [Raft](#raft) byzantinisch fehlertolerant, weshalb der Algorithmus auch etwas komplexer ist wie Raft (Quelle: https://ancapalex.medium.com/a-cursory-introduction-to-byzantine-fault-tolerance-and-alternative-consensus-1155a5594f18).

Weitere Details und die Funktionsweise können unter https://de.wikipedia.org/wiki/Paxos_(Informatik) nachgelesen werden.

### PBFT - Practical Byzantine Fault Tolerance
Beim PBFT handelt es sich um einen Algorithmus der nicht auf rechenintensive PoW (Proof of Work) Operationen setzt, sondern das ganze Thema effizienter angeht. Die Funktionsweise des Algorithmus ist unter https://www.geeksforgeeks.org/practical-byzantine-fault-tolerancepbft/ beschrieben.

Weitere Details und weitere "Byzantine Fault"-tolerante Algorithmen und Anwendungen sind im Wikipedia-Artikel https://en.wikipedia.org/wiki/Byzantine_fault#Solutions nachzulesen.

<!--
https://www.vpnunlimited.com/de/help/cybersecurity/byzantine-fault-tolerance
//-->

