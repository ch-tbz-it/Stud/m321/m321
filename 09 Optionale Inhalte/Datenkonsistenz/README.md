# Datenkonsistenz - mit Fokus auf Datenbanken

[TOC]

## CAP Theorem
Das CAP-Theorem besagt, dass bei einem Ausfall eines Systems (CA**P**: **P**artition tolerance) - beispielsweise einer Datenbank bzw. Teilen davon - es nur möglich ist entweder das System weiter verfügbar zu halten (C**A**P: **A**vailability) indem Schreibvorgänge weiterhin durchgeführt werden (mit dem Risiko von Inkonsistenzen in den Daten) oder indem die Konsistenz (**C**AP: **C**onsistency) der Daten gewährleistet wird, indem Schreibvorgänge abgelehnt werden, solange unklar ist, wer die aktuellste Version der Datenbasis besitzt.

Fazit ist: Wenn die Datenkonsistenz das oberste "Gut" sein soll, dann geht dies immer zulasten von der Verfügbarkeit.

Weitere Details & Quelle zum CAP-Theorem: https://en.wikipedia.org/wiki/CAP_theorem

## Datenkonsistenz
Um eine Datenkonsistenz zu gewährleisten ist entscheidend, dass genau bestimmt werden kann, in welcher Reihenfolge Ereignisse geschehen bzw. geschehen sind. Folgendes Beispiel verdeutlicht die Problematik:

Es existiert eine Datenbank, die auf einem Master-Node und zwei Slave-Nodes verteilt liegt. Es existiert folgende Tabelle mit Produktdaten:

| ID | Produkt | Preis    |
|----|---------|----------|
| 1  | Laptop  | 1500 CHF |
| 2  | Maus    | 15 CHF   |

Nun wird beispielsweise ein weiteres Produkt auf dem Master eingefügt:

| ID | Produkt  | Preis    |
|----|----------|----------|
| 1  | Laptop   | 1500 CHF |
| 2  | Maus     | 15 CHF   |
| 3  | Tastatur | 30 CHF   |

Auf den beiden Replikas (Slave-Nodes) ist direkt nach Abschluss des Schreibvorgangs die Tastatur noch nicht vorhanden, sondern muss erst noch auf die beiden Replikas übertragen werden. Wenn direkt im Anschluss die Applikation nun von einem Replika die Anzahl der Artikel ausliest, dann wird das Resultat 2 sein. Würde vom Master gelesen wäre das Resultat 3. Wenn jetzt dieses Resultat in einer anderen Form wiederverwendet würde - beispielsweise zur Berechnung der nächsten freien ID im Sinne von "anzahl Produkte + 1" - dann würde beim nächsten Insert erneut versucht ein Produkt mit der ID 3 auf den Master zu schreiben und es kommt zu einem Fehler wegen eines doppelt vorhandenen Keys.

Wenn die gelesenen Daten in Schreiboperationen wiederverwendet werden, dann müssen diese von einer "sauberen" Kopie der Daten gelesen werden. D. h. es muss sichergestellt werden, dass der Kopiervorgang vor dem Lesevorgang abgeschlossen ist, damit keine Inkonsistenz entsteht. Wird trotzdem von "unsauberen" Kopien gelesen, dann spricht man in der Informatik vom sogenannten [dirty read](https://de.wikipedia.org/wiki/Schreib-Lese-Konflikt).

Die Anforderungen an die Konsistenz können je nach konkretem Anwendungsfall sehr stark unterschiedlich sein. Deshalb gibt es verschiedene Grade und Arten wie konsistent ein System ist bzw. sein muss. Weitere Details zu dem Thema sind im Wikipediaartikel zur [Konsistenz (Datenspeicherung)](https://de.wikipedia.org/wiki/Konsistenz_(Datenspeicherung)) beschrieben.

### Transaktionsmanagement
Die Idee von Transaktionen ist, dass mehrere Operationen unteilbar miteinander verknüpft werden und entweder als ganzes oder gar nicht ausgeführt werden. Dadurch sollen voneinander abhängige Schreib-Lese-Schreib-Vorgänge so ausgeführt werden, dass keine Inkonsistenzen entstehen können.

Transaktionen müssen den sogenannten ACID-Kriterien genügen:
* **A**tomicity - Die einzelnen Operationen innerhalb der Transaktion müssen unteilbar miteinander verbunden sein. Entweder werden alle Operationen oder bei Fehlern keine der Operationen ausgeführt. Um das sicherzustellen, existiert für den Fehlerfall eine sogenannte Rollback-Routine, die die gemachten Änderungen auf der Datenbank wieder rückgängig macht.
* **C**onsistency - Am Ende der Transaktion muss wieder ein konsistenter Zustand erreicht sein.
* **I**solation - Parallel laufende Transaktionen dürfen sich gegenseitig nicht beeinflussen.
* **D**urability - Die Daten müssen nach erfolgreicher Ausführung "dauerhaft" auf der Datenbank gespeichert sein. Selbst wenn das System ausfällt während der Schreibvorgang noch nicht überall abgeschlossen wurde. Dies wird durch Transaction-Logs erreicht.

Quelle & weitere Details: https://de.wikipedia.org/wiki/ACID

<!--
Distributed Transaction: Two-Phase Commit (2PC) oder Three-Phase Commit (3PC)
//-->

## Zeitsynchronisation
Um bestimmen zu können, in welcher Reihenfolge Ereignisse stattgefunden haben, ist es manchmal erforderlich, dass die Teile des verteilten Systems über eine gemeinsame Zeit verfügen. Es kann sich dabei um die effektive Uhrzeit oder aber auch um eine logische Zeit handeln. Bei der logischen Zeit erhält beispielsweise jedes Ereignis eine Nummer. Je später ein Ereignis stattfindet, desto höher ist die Nummer. So werden die Ereignisse in eine logische Reihenfolge gebracht.

Um die tatsächliche Uhrzeit synchron zu halten wurde das [Network Time Protocol (NTP)](https://de.wikipedia.org/wiki/Network_Time_Protocol) entwickelt, welches heute in den meisten Systemen für die Synchronisation der Uhrzeit eingesetzt wird.

## Konsensalgorithmen
Um in Konfliktsituationen wieder zu einem gemeinsamen Datenstand bzw. einem gemeinsamen Vorgehen zu kommen, werden sogenannte Konsensalgorithmen angewandt. Weitere Details zu [Konsensalgorithmen](../Konsensalgorithmen) sind in einer eigenen Theorie-Einheit erklärt.

<!--
https://www.studysmarter.de/schule/informatik/theoretische-informatik/verteilte-datenbanken/

#### Eventuelle Konsistenz
über:
- Conflict-free Replicated Data Types (CRDTs)
- Vector Clocks
- Anti-Entropy-Protokolle

### Sicherstellung Datenkonsistenz
Durch Replikation (Master-Slave-Replikation oder Multi-Master-Replikation)
Quorum-basierte Systeme
Write-Ahead Logging (WAL)
Paxos/Raft-Protokoll

## Konfliktbehebungsstrategien

### Konsens-Algorithmen
Unter Konsens-Algorithmen werden Algorithmen zusammengefasst, die zum Ziel haben, das Vorgehen im
//-->
