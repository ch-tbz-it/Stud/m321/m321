# Blockchain

[TOC]

## Einleitung
Bei konkreten Blockchain-Umsetzungen (beispielsweise Bitcoin oder Ethereum) handelt es sich um teilweise sehr grosse verteilte Systeme. Jeder, der beispielsweise mit Bitcoins Transaktionen durchführt, ist Teil des einen weltweit verteilten Systems.

## Grundidee / Aufbau
Die Grundidee ist, dass bei der Blockchain Blöcke aneinander gekettet werden. Dies geschieht, indem der Hash-Wert des vorhergehenden Blocks zusammen mit dem Blockinhalt des aktuellen Blocks verwendet werden, um den Hash-Wert des aktuellen Blocks zu bilden. Dadurch entsteht eine Abhängigkeit des aktuellen Blocks vom vorhergehenden. Das bedeutet, dass die Blöcke nur in der Reihenfolge gültig sind und die Blöcke nicht miteinander vertauscht werden können. Würden die Blöcke in der Reihenfolge vertauscht, dann würden die Hash-Werte ungültig und die Manipulation könnte erkannt werden.

## Anwendungsfall Bitcoin
Bitcoin ist eine bekannte Kryptowährung, die auf der Blockchain-Technologie basiert (d. h. diese verwendet). Die Grundidee von Bitcoin ist, dass jede Transaktion in einem Block in der Blockchain abgelegt wird. Jeder, der Bitcoin bei sich installiert hat, erhält eine Kopie der kompletten Blockchain. Diese enthält alle Transaktionen seit Beginn von Bitcoin. Das sind heute bereits enorm viele Daten und die Datenmenge wächst stetig - mit jeder neuen Transaktion, also jeder Übertragung von Bitcoind von einem Wallet zu einem anderen Wallet.

Ein Wallet ist bei Bitcoin das digitale Konto - im Prinzip ein Public/Private-Key-Paar, welches den Inhaber eines Kontos mit seinem Konto verbindet. Wenn der Private-Key verloren geht, dann ist folglich auch der Zugriff auf das Bitcoin-Konto verloren.

Wenn nun eine neue Transaktion gemacht werden soll, dann muss überprüft werden, dass diese Transaktion gültig ist - sprich vom tatsächlichen Inhaber des Wallets kommt. Dies wird bei Bitcoin mit einem sogenannten Proof of Work vorgenommen. Das Proof of Work ist eine sehr rechenintensive Angelegenheit. Der Erste, der diesen Proof of Work korrekt berechnet hat, erhält als Belohnung Bitcoins. Geld verdienen durch die Berechnung von Proof of Work wird als Mining oder auch Kryptomining bezeichnet. 

Alle weiteren Teilnehmer im Bitcoin-Netzwerk müssen nun überprüfen ob der Proof of Work des ersten Teilnehmers korrekt ist, bevor die Transaktion der Blockchain angehängt wird. Die Überprüfung ist dann nicht mehr ganz so rechenintensiv. Wichtig ist aber, dass mehr wie 50% des gesamten Bitcoin-Netzwerks den Proof of Work als Richtig bestätigen. In dem Zusammenhang existiert auch ein bis heute nicht gelöstes Sicherheitsproblem. Wenn ein Angreifer mehr wie 50% des Bitcoin-Netzwerks unter seiner Kontrolle hat, dann kann er eine x-Beliebige Transaktion an die Blockchain hängen ohne das auffällt, dass es sich dabei um eine gefälschte Transaktion handelt. Weil das Bitcoin-Netzwerk sehr gross ist, ist es unwahrscheinlich, dass bei diesem ein solcher Angriff erfolgreich sein wird - ausschliessen kann man es aber auch nicht.

## Quellen und weitere Details
* https://hub.hslu.ch/informatik/blockchain-einfach-erklaert/: Artikel der HSLU (Hochschule Luzern), der die Funktionsweise von Blockchains sehr gut erklärt
* https://tangem.com/de/blog/post/how-the-blockchain-gets-hacked-attacks-on-decentralized-networks/: Blog-Post über mögliche Angriffsszenarien auf die Blockchain