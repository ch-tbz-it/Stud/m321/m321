# Optionale Inhalte
Dieses Kapitel enthält Inhalte, die bei Interesse optional erarbeitet werden können. Es handelt sich dabei meist um Wunschthemen von Lernenden die sich in einem mit dem Modul verwandten Thema gerne weiter vertieft hätten. 

Gibt es Themen, die Sie im Modul vermissen, die mit verteilten Systemen zu tun haben und die Sie gerne vertiefen würden? Dann melden Sie sich beim Modulverantwortlichen (siehe [Hauptseite](../README.md)) mit der Bitte Ihr Wunschthema im Modul mit aufzunehmen.

Haben Sie allenfalls selbst Inhalte zu einem Thema erstellt, welche zum Modulthema passenden und würden diese Inhalte gerne mit anderen Lernenden teilen? Dann lassen Sie Ihre Unterlagen (am besten in Markdown) dem Modulverantwortlichen zukommen. Dieser entscheidet dann über die Aufnahme im Modulrepository Ihrer Inhalte.

## Überblick über optionale Inhalte
* [Docker / Kubernetes Refresher & Vertiefung](./Docker_Kubernetes/README.md)
* [Datenkonsistenz - mit Fokus auf Datenbanken](./Datenkonsistenz)
* [Konsensalgorithmen](./Konsensalgorithmen) - Algorithmen die es den Teilen in einem verteilten System erlauben bei Konflikten zu einer gemeinsamen Lösung zu gelangen
* [Blockchain](./Blockchain) - ein spezieller Anwendungsfall für ein verteiltes System und von Konsensalgorithmen